{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

import GHC.Generics (Generic)
import qualified Data.ByteString.Lazy as LB (readFile)
import Data.Aeson ( eitherDecode, FromJSON, Value, ToJSON )

data Foo = Foo
  { foo :: Int
  , bar :: String
  }
  deriving (Show, Generic, ToJSON, FromJSON)

main :: IO ()
main = do
  json <- LB.readFile "app/data/test-data.json"
  let objects = decoder (eitherDecode json :: Either String [Value])
  print (map show objects)

decoder :: Either a b -> b
decoder (Left _) = error "Error"
decoder (Right x) = x
